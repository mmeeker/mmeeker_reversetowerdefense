﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnManager : MonoBehaviour
{
    public static SpawnManager instance;

    public Text attackerText;

    public int attackerLimit = 3;
    public int attackerNumber = 0;

    public GameObject firstAttackerPrefab;
    public GameObject secondAttackerPrefab;
    public GameObject thirdAttackerPrefab;

    private GameObject attackerToSpawn;

    private void Awake()
    {
        if (instance != null)
            return;
        instance = this;
    }

    public GameObject GetAttackerToSpawn()
    {
        return attackerToSpawn;
    }

    public void SetAttackerToSpawn(GameObject attacker)
    {
        attackerToSpawn = attacker;
    }
}
