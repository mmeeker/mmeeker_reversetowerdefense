﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    SpawnManager spawnManager;

    public Button firstAttackerButton;
    public Button secondAttackerButton;
    public Button thirdAttackerButton;

    public Text firstText;
    public Text secondText;
    public Text thirdText;


    private void Start()
    {
        spawnManager = SpawnManager.instance;
    }
    public void PurchasedFirstAttacker() {
        spawnManager.SetAttackerToSpawn(spawnManager.firstAttackerPrefab);
        firstAttackerButton.GetComponent<Image>().color = Color.gray;
        secondAttackerButton.GetComponent<Image>().color = Color.white;
        thirdAttackerButton.GetComponent<Image>().color = Color.white;
    }

    public void PurchasedSecondAttacker()
    {
        spawnManager.SetAttackerToSpawn(spawnManager.secondAttackerPrefab);
        secondAttackerButton.GetComponent<Image>().color = Color.gray;
        firstAttackerButton.GetComponent<Image>().color = Color.white;
        thirdAttackerButton.GetComponent<Image>().color = Color.white;
    }

    public void PurchasedThirdAttacker()
    {
        spawnManager.SetAttackerToSpawn(spawnManager.thirdAttackerPrefab);
        thirdAttackerButton.GetComponent<Image>().color = Color.gray;
        secondAttackerButton.GetComponent<Image>().color = Color.white;
        firstAttackerButton.GetComponent<Image>().color = Color.white;
    }

    public void UpgradeAttackeLimit()
    {
        if (GameController.gc.currency - 250 >= 0)
        {
            GameController.gc.currency -= 250;
            spawnManager.attackerLimit += 1;
            spawnManager.attackerText.text = "Attackers " + spawnManager.attackerNumber + "/" + spawnManager.attackerLimit;
        }
    }

    public void UpdateText(int i)
    {
        if (i == 0)
            firstText.text = "LV: " + GameController.gc.a1Level;
        else if (i == 1)
            secondText.text = "LV: " + GameController.gc.a2Level;
        else if (i == 2)
            thirdText.text = "LV: " + GameController.gc.a3Level;
    }
}
