﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class StartCube : MonoBehaviour
{
    SpawnManager spawnManager;

    public Color hoverColor;

    public List<GameObject> attackerList = new List<GameObject>();
    private int listIndex = 0;

    public Button button;
    public Text Text1;
    public Text Text2;
    public Text Text3;
    public Image image1;
    public Image image2;
    public Image image3;    

    private Renderer rend;
    private Color startColor;
    
    private int A1count = 0;
    private int A2count = 0;
    private int A3count = 0;

    private void Start()
    {
        
        
        button.interactable = false;
        Text1.text = "";
        Text2.text = "";
        Text3.text = "";
        image1.gameObject.SetActive(false);
        image2.gameObject.SetActive(false);
        image3.gameObject.SetActive(false);

        rend = GetComponent<Renderer>();
        
        startColor = rend.material.color;
        spawnManager = SpawnManager.instance;
        spawnManager.attackerText.text = "Attackers " + spawnManager.attackerNumber + "/" + spawnManager.attackerLimit;
    }

    private void OnMouseDown()
    {
        if (spawnManager.attackerNumber < spawnManager.attackerLimit)
        {
            if (spawnManager.GetAttackerToSpawn() == null)
                return;

            GameObject attacker = spawnManager.GetAttackerToSpawn();
            attackerList.Add(attacker);

            if (attacker.name == "Helicopter")
            {
                A1count += 1;
                spawnManager.attackerNumber += 1;
                image1.gameObject.SetActive(true);
                Text1.text = "        x" + A1count;
            }
            if (attacker.name == "HoverTank")
            {
                A2count += 1;
                spawnManager.attackerNumber += 1;
                image2.gameObject.SetActive(true);
                Text2.text = "        x" + A2count;
            }
            if (attacker.name == "Buggy")
            {
                A3count += 1;
                spawnManager.attackerNumber += 1;
                image3.gameObject.SetActive(true);
                Text3.text = "        x" + A3count;
            }
            spawnManager.attackerText.text = "Attackers " + spawnManager.attackerNumber + "/" + spawnManager.attackerLimit;
            button.interactable = true;

            listIndex += 1;
        }
    }

    private void OnMouseEnter()
    {
        if(spawnManager.GetAttackerToSpawn() == null)
            return;
        rend.material.color = hoverColor;
    }    

    private void OnMouseExit()
    {
        rend.material.color = startColor;
    }

    public void ClearList()
    {
        attackerList.Clear();
        button.interactable = false;
        Text1.text = "";
        Text2.text = "";
        Text3.text = "";

        image1.gameObject.SetActive(false);
        image2.gameObject.SetActive(false);
        image3.gameObject.SetActive(false);

        spawnManager.attackerNumber -= A1count;
        spawnManager.attackerNumber -= A2count;
        spawnManager.attackerNumber -= A3count;

        A1count = 0;
        A2count = 0;
        A3count = 0;
        spawnManager.attackerText.text = "Attackers " + spawnManager.attackerNumber + "/" + spawnManager.attackerLimit;
    }
}
