﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController gc;
    public GameObject secondTurrent;
    public GameObject thirdTurrent;

    public Text currencyText;

    public float[] attackerHealth;
    public float[] attackerSpeed;

    public int a1Level = 1;
    public int a2Level = 1;
    public int a3Level = 1;

    public int currency = 500;
    public int upgradeCost = 200;

    void Awake()
    {
        if(gc ==null)
        {
            gc = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
            return;
        }
       
    }

    // Update is called once per frame
    void Update()
    {
        currencyText.text = "$" + currency;
    }

    public void UpgradeAttacker(int id)
    {        
        if (currency-upgradeCost >= 0)
        {
            currency -= 200;
            if (id == 0)
            {
                if (a1Level % 2 == 0)
                    attackerHealth[id] += 1;
                else
                    attackerSpeed[id] += 1f;       
                a1Level++;
            }
            if (id == 1)
            {
                if (a2Level % 2 == 0)
                    attackerHealth[id] += 1;
                else
                    attackerSpeed[id] += 1f;       
                a2Level++;

            }
            if (id == 2)
            {
                if (a3Level % 2 == 0)
                    attackerHealth[id] += 1;
                else
                    attackerSpeed[id] += 1f;            
                //add stun duration increase
                a3Level++;

            }
        }
    }

    public void spawnTurrent(GameObject turrent)
    {
       turrent.SetActive(true);
    }
}
