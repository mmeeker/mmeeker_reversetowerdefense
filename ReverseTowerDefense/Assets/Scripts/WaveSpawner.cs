﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour
{
    SpawnManager spawnManager;
    StartCube startCube;

    public Button attackerLimitButton;
    public Button nextButton;
    public Text waveText;
    public Text endText;
    public Text infoText;
    public Text helperText;

    public float timeBetweenWaves = 0f;

    private bool waveReady = true;
    private int waveNumber = 1;
    

    private void Start()
    {
        spawnManager = SpawnManager.instance;
        startCube = GetComponent<StartCube>();
    }

    private void Update()
    {
        if (timeBetweenWaves > 0f)
        {
            timeBetweenWaves -= Time.deltaTime;
            nextButton.interactable = false;
        }
        else if (timeBetweenWaves <= 0f)
        {
            if (!waveReady)
                waveNumber++;
            nextButton.interactable = true;
            waveReady = true;
        }
        waveText.text = "Wave " + waveNumber + "/X";

        if (waveNumber == 2)
        {
            attackerLimitButton.interactable = true;
            infoText.gameObject.SetActive(false);
            helperText.gameObject.SetActive(false);
        }
        else if (waveNumber == 4)
            GameController.gc.spawnTurrent(GameController.gc.secondTurrent);
        else if (waveNumber == 7)
            GameController.gc.spawnTurrent(GameController.gc.thirdTurrent);
        else if (waveNumber == 8)
            endText.gameObject.SetActive(true);
    }

    public void WaveCountdown()
    {

        if (waveReady)
        {
            StartCoroutine(SpawnWave());
            waveReady = false;
            timeBetweenWaves = 20f;
        }
    }

    IEnumerator SpawnWave()
    {

        startCube.button.interactable = false;
        startCube.Text1.text = "";
        startCube.Text2.text = "";
        startCube.Text3.text = "";
        startCube.image1.gameObject.SetActive(false);
        startCube.image2.gameObject.SetActive(false);
        startCube.image3.gameObject.SetActive(false);
        for (int i = 0; i < startCube.attackerList.Count; i++)
        {
            SpawnAttacker(i);
            yield return new WaitForSeconds(0.8f);
        }
        startCube.ClearList();
         //waveNumber++;
    }

    void SpawnAttacker(int i)
    {
        Instantiate(startCube.attackerList[i], transform.position, startCube.attackerList[i].transform.rotation);
    }
}