﻿using UnityEngine;
using UnityEngine.UI;

public class Attacker : MonoBehaviour{

    public AudioSource hitSound;

    public float speed = 10;
    public float health = 5f;
    public int id;

    private int startPosition;
    private Transform target;
    private int waypointIndex = 0;

    void Start ()
    {
        health = GameController.gc.attackerHealth[id];
        speed = GameController.gc.attackerSpeed[id];
        startPosition = (int)gameObject.transform.position.x;
      
        if (startPosition == 5)
        {
            target = Waypoints.points[0];
            waypointIndex = 0;
        }
        else if (startPosition == 35)
        {
            target = Waypoints.points[1];
            waypointIndex = 1;
        }
        else if (startPosition == 70)
        {
            target = Waypoints.points[2];
            waypointIndex = 2;
        }
    }

    void Update ()
    {
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, target.position) <= 0.4f)
            GetNextWaypoint();   
    }

    void GetNextWaypoint()
    {
        if (waypointIndex >= Waypoints.points.Length - 1)
        {
            GameController.gc.currency += 100;
            Destroy(gameObject);
            return;
        }

        if (waypointIndex == 1)
            waypointIndex = 3;
        else if (waypointIndex == 2)
            waypointIndex = 1;
        else
            waypointIndex++;
        target = Waypoints.points[waypointIndex];
    }

    public void takeDamage()
    {
        health -= 1;
        hitSound.Play();

        if (health <= 0)
            Destroy(gameObject);
    }

    
}
